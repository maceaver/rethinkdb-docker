#!/bin/bash
PATH=/opt/rethinkdb/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

echo "rethinkdb Dump started: $(date)"

/usr/local/bin/rethinkdb-dump -c db -f /backup/`date +\%Y\%m\%d\%H\%M\%S`.tar.gz

echo "rethinkdb finished: $(date)"
